<!-- Note that a single bug report must describe exactly one bug. If there are more than one bug, then as many separate bug reports should be filed. -->

<!-- Provide a general summary of the issue in the Title above -->

<!-- Write your details after each explaination -->

## Operating system
<!-- Your operating system and the precise version (include the Linux distribution, or the version of Windows). -->


## VLC Media player version
<!-- Are you using the latest version of VLC? Check the wiki please https://wiki.videolan.org/Report_bugs -->


## Expected Behavior
<!-- What should happen -->


## Current Behavior
<!-- What happens instead of the expected behavior -->


## Possible Solution
<!-- Not obligatory, but suggest a fix/reason for the bug, -->


## Steps to Reproduce
<!-- Provide a link to a live example, or an unambiguous set of steps to -->
<!-- reproduce this bug. Include code to reproduce, if relevant -->
1.
2.
3.
4.


## Log
<!-- Logs help us to understand what's going on. As those from messages dialog, verbosity to 2 (GUI mode) or the -vvv switch (CLI mode). Paste it between ``` ``` -->
```

```

## Reproducibility of the problem
<!-- Does it occur every time? If not, how often? -->


## Type and format
<!-- If applicable, the type (movie, music, subtitle) and the format of the file being played. If known, also the codec(s) in the file. If possible, the video sample in question. -->

## Anything else?
<!-- Is there anything else we didn't included? If yes, please tell us -->